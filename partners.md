---
title: partners
header: our partners
layout: page
next:
 url: team.html
 menu: Team
---

## Our partners are :

- [Caligenix][1]
- [SanGiovanni Laboratory][2]
- [Doctor I·T][3]

[1]: https://www.caligenix.com/
[2]: https://nutrientscience.org/
[3]: https://www.drit.ml

