---
layout: post
status: draft
date: 2018-12-20
author:
 name: "Colleen F. Draper, Ph.D."
 bio: colleen

title: "Doctor Col, Thesis metabolic signatures in nutrition health sexual dimortphism and hormone chronobiology"
slug: metabolic-signatures-in-nutrition-health-sexual-dimortphism-and-hormone-chronobiology
teaser-img: /ipfs/QmYauW48MVAzMTJ5j2kQdvLwEkPFmYYzHxQTkG1CgUPGA4/colleen-photo.jpg
preview-img: https://cdn.statically.io/img/dweb.link/ipfs/QmYauW48MVAzMTJ5j2kQdvLwEkPFmYYzHxQTkG1CgUPGA4/colleen-thesis-graphic.png
permalink: /blog/:slug/
excerpt: 
description: 
output: false
tags: fruit, yellow, blue
category: food
path-to-root: ../../
thesis-url: https://dweb.link/ipfs/QmYauW48MVAzMTJ5j2kQdvLwEkPFmYYzHxQTkG1CgUPGA4/cfd-thesis-full.pdf
ref: http://hdl.handle.net/1887/68234
---
Personalized  nutrition, which originatedfrom  gene  and  nutrient
interaction  research [[1]], has received  much  attention  in
health-oriented  marketplaces  due  to increased technological
capabilities    to    sensitively    measure unique    differences
between individuals.  Public  interest  in individualizing  health  has
grown  as  various  sectors  in society offered   more   and   improved
technologies   and   capabilities to   empower individuals  to  move
from  one-size-fits-all  to the  ability  to  quantify  and  controltheir
personal lives.  Consequently, new research must be designed to assess
individualized health  trajectories  and  optimize  individualized
diagnostic and  therapeutic  decision-making [[2], [3]]. In orderto realize
the power of personalized nutrition, research needs to be  conducted
on healthypeople with  highly  sensitive  metabolic  markers capable  of
diagnosingthe  impact  of  environmental  stressors,  such  as  diet,  and
physiologic stressors, such as changing sex hormones and the menstrual
cycle. Obvious phenotypic differences,  such  as  sex  and  gender,
should  be  evaluated  as  they  representeasily targeted subtypes with
unique  physiologies  for  which  personalized  nutrition  can  be
prescribed.

The   power   of   personalized   health   diagnosis   from which
personalized   nutrition therapies can be prescribed, relies not
only on individual variations in genotype, but the environmental
and physiologicresponses    manifested    through    transcriptomic,
metabolomicand proteomic measurements,as well as personal psycho-social
factors, that can be measuredand modified.

The aim of this [thesis][4] was to study healthy women and men and their
metabolic response to nutrition and/or natural hormonedynamics, monitored
by  clinical  and  metabolomics biomarkers,  as  a  way  to  better
understand human metabolic health.

[[...]({{page.ref}})]



[1]: https://dweb.link/ipfs/QmWEXWgkA21iYqk1GtZojyEaZbx9gCDhgeeGGYjKxF1PXD/debusk2005.pdf
[2]: https://dweb.link/ipfs/QmbkuftAV5aARZFTWBrY9LtXmah6qssUhXM1RHcJF4YPb4/schork2016.pdf
[3]: https://dweb.link/ipfs/Qmf8PWjtMoLyZgPDvHcFqU8KntFTNoGm274ngwqMad6c2b/kaput2012.pdf
[4]: pub/cfd-thesis-full.pdf
