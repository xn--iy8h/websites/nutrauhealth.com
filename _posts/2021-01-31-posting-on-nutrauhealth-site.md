---
title: "Posting articles on nutraU health website"
author:
 name: "Michel G. Combes, Ph.D."
 bio: michel
 url: https://www.drit.ml
slug: posting-articles-on-nutrau-website
teaser: posting.png
permalink: /blog/:slug/
#excerpt: 
description: 
draft: true
output: false
tags: admin, article, posting
category: website
path-to-root: ../../
layout: post
---
### This site has 3 articles feeds :

 1. [New U Ideas](new-u-ideas.html),
 2. [Video logs](media.html),
 3. [blogs](vblog.html),
 4. drafts (compile w/ --draft, and check [post](blog/)).

![posting](assets/img/posting.png)
<style>img[alt="posting"] { float: right; }</style>

### How to post :

Posting is done via [GitLab][repo] placing a markdown file in the respective folder, except for videos which are
listed in a media.yml file

 1. [_new-u-ideas](https://gitlab.com/xn--iy8h/websites/nutrauhealth.com/-/tree/master/_new-u-ideas)
 2. [_data/media.yml](https://gitlab.com/xn--iy8h/websites/nutrauhealth.com/-/blob/master/_data/media.yml)
 3. [_posts](https://gitlab.com/xn--iy8h/websites/nutrauhealth.com/-/tree/master/_posts)
 5. [_drafts](https://gitlab.com/xn--iy8h/websites/nutrauhealth.com/-/tree/master)


The git page is at <https://🕸.gitlab.io/websites/nutrauhealth.com/>
and the netlify page is at [{{site.netname}}](https://{{site.netname}}.netlify.app)


[repo]: https://gitlab.com/xn--iy8h/websites/nutrauhealth.com

The site has also 2 other pages for announcements:

 1. [news](news.html),
 2. [products](products.html),




