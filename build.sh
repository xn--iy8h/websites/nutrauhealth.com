#
tic=$(date +%s)
find . -name "*.*~1" -delete
jekyll build
qm=$(ipfs add -Q -r _site)
gwport=$(ipfs config Addresses.Gateway | cut -d'/' -f 5)
echo url: http://localhost:$gwport/ipfs/$qm
echo url: https://gateway.pinata.cloud/ipfs/$qm
echo $tic: $qm >> qm.log
git add qm.log

