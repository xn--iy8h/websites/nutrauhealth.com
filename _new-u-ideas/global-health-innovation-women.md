---
layout: post
path-to-root: ../

header: New U Ideas
title: Global Health Innovation for Women
subtitle: Vaginal microbiome and natural therapeutics
teaser-img: int-women-health.jpg
preview-img: assets/img/sun-rise-on-earth.jpg
summary: this a summary for this article

date: Sun Jan 31 08:57:13 AM CET 2021
author:
 name: "Dr. Colleen Fogarty Draper, R.D."
 bio: colleen

ref: http://duckduckgo.com/?q=Global+Health+Innovation+for+Women+Vaginal+microbiome+and+natural+therapeutics+!g
---
The vaginal microbiome has been decoded [[1]].
What does this mean? The vaginal microbiome plays a significant role in maintaining mucosal health, disease protection and optimizing birth outcomes. *Lactobacillus crispatus* seems to be important for vaginal health and *Lactobacillus* diversity is of utmost importance to prevent such problems as bacterial vaginosis and sexually transmitted diseases (including HIV).

Estrogen protects this diversity and promotes *Lactobacillus* stability. Diet plays a role through its effect on the gut microbiome [[2]].
More research is needed on diet and  probiotic impact to understand best therapies [[3]].

The skin microbiome may be a reflection of the gut microbiome which impacts the vaginal microbiome [[4]]. Ultimately, we will be able to determine the health of the vagina with a skin tape strip for easy, non-invasive microbiome analysis [[5]]. A clinical research program should be designed to substantiate and validate the interconnection of these ideas. In the future, this non-invasive skin based diagnostic could be used to determine optimal ingestible and topical therapies to improve women's vaginal health, prevent disease, and optimize birth outcomes globally. 

Read [more]

[1]: https://www.scientificamerican.com/article/decoding-the-vaginal-microbiome/
[2]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6743080/.
[3]: https://www.health.harvard.edu/blog/should-you-use-probiotics-for-your-vagina-2019122718592
[4]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6920876/
[5]: https://www.frontiersin.org/articles/10.3389/fmicb.2018.02362/full
[more]: {{page.ref}}
