---
layout: post
path-to-root: ../

title: We expect more women ...
subtitle: in the health sector than ever
modal: true
draft: true
header: New U Ideas
teaser-img: sun-rise-on-earth.jpg
preview-img: 'https://cdn.statically.io/img/dweb.link/ipfs/QmaASi2cP4GDh5yywB3UyWDpyLGpJhBmFsrXNgTgW7k7Su'
#excerpt: this a summary for this article

date: Sat Jan 30 07:56:11 AM CET 2021
author: "Michel G. Combes, Ph.D."

ref: http://duckduckgo.com/?q=What+You+weren't+expected+a+woman?
---
With the lockdown and remote-work-from-home situation, we expect mode women to show up in the health industry.

Read [more]

[more]: {{page.ref}}

