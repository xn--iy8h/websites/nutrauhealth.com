---
title: BRINGING REAL SCIENCE TO SUPPLEMENTS
subtitle: "Caligenix release: Immunotype"
author:
 name: "Michel G Combes, Ph.D."
 bio: michel
slug: caligenix-immunotype
date: Thu Jan 28 06:13:37 AM CET 2021
teaser: immunotype.jpg
preview-img: assets/img/immunotype.jpg
excerpt: "Immunotype was created in response to the pandemic—now more than ever, it’s crucial that we protect and supplement our immune systems."
description: Vitamin D3, Vitamin C, Vitamin A, Vitamin B6, Vitamin B12, Folate, Zinc, Selenium, Copper, Echinacea, Quercetin, Turmeric, Elderberry.
output: false
tags: immunity, supplements
category: nutrition, personalized
path-to-root: ../
layout: post
---
DNA-based and evidence-backed,
Immunotype works in tandem with your body’s innate ability to
heal—because you deserve a vitamin as intelligent as your immune system.

Immunotype was created in response to the pandemic—now more than ever,
it’s crucial that we protect and supplement our immune systems. We
sought out to create a product that actually works to keep our communities
safe and healthy. Additionally, we’re committed to donating 5% of all
Immunotype proceeds to provide supplementation to those directly affected
by the virus. We’re in this together.

Using only the purest, most effective bioavailable ingredients is of
paramount importance to us, so we searched high and low to find suppliers
that aligned with our values and standards. It took a few trips around
the globe, but we found them.

```{.text-black}
Vitamin D3
Vitamin C
Vitamin A
Vitamin B6
Vitamin B12
Folate

Zinc
Selenium
Copper

Echinacea
Quercetin
Turmeric
Elderberry
```

As pioneers in the genetic testing space, we compiled decades of DNA
testing data to find immunity boosting ingredients that universally
absorb and thus bypass our unique genetic variants. Then we integrated
these ingredients and put them in a bottle. Meet Immunotype.


learn more : see [Caligenix](https://caligenix.com/pages/ingredients)

