---
layout: post
modal: true
draft: true
path-to-root: ../

title: AI in personalized nutrition
subtitle:
header: New U Ideas
teaser-img: posting.png
preview-img: https://cdn.statically.io/img/dweb.link/ipfs/QmWcBA47ubHWYwiWqiWmHTJoMNVBLztahxvizGAjvsCqHc
#excerpt: this a summary for this article

date: Sun Jan 31 08:56:25 AM CET 2021
author:
 name: "Michel G. Combes, Ph.D."
 bio: michel
 email: michelc@gc-bank.org


ref: http://duckduckgo.com/?q=What+You+weren't+expected+a+woman?
---

With the overwelming amount of data available, we can't comprehend any more its volume
and learn from it. That is where computers excel; Indeed they are good to extract the needle from the hay stackC

For personalized nutrition we collect a large set of mood, symptoms, health metrics and have AI correlate them
into a distributed database. Then any query with your own personalized parameters will point to a crafted response
that suit you best.

Read [more]

[more]: {{page.ref}}

