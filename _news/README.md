---
path-to-root: ../../
---
# README 


This is folder is where news announcement are made.

Simply use a text file with the .md extension in "markdown" format.

resulting post is published under [/news/:slug][1] where *:slug* is
a variable specified in the front matter to define the name the published file.




[1]: https://www.nutrauhealth.com/news.html

