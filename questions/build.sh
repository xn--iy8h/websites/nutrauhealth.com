#

set -e
libreoffice --convert-to csv "Questionnaire.ods"
perl csv2form.pl "Questionnaire.csv"
qm=$(ipfs add -w Questionnaire.ods form.html -Q)
curl -I https://ipfs.blockringtm.ml/ipfs/$qm
echo url: https://ipfs.blockringtm.ml/ipfs/$qm
