---
modal: true
layout: post
path-to-root: ../

title: "Doctor Col™ branding"
date: "Sat Feb  6 07:32:33 AM CET 2021"
header: "doctor col branding"
slug: doctor-col-branding
author:
 bio: michelc
 name: "Dr. Mic"

teaser-img: /ipfs/Qmf9EYygJQQrJYiBmkudhPMvF6KnbKcvznmoxfCxa4zuk3/branding.jpg
preview-img: https://cdn.statically.io/img/dweb.link/ipfs/Qmf9EYygJQQrJYiBmkudhPMvF6KnbKcvznmoxfCxa4zuk3/branding.jpg
tags: "#DRCOL, Dr. Col Branding"

permalink: /drafts/:slug.html
blog:
 url: blog.html
 menu: Blog
---
# Doctor Col branding

Well if we want to have everyone know about "[Dr. Col™][1]" brand we have to talk about it and let the [AI][2] knows we are out there.
How we achieve that first our website [{{site.url}}][2] must have some sort of mention of both phases "[Dr. Col][1]" and "[Doctor Col][3]",
indeed a simple [google seach][4] should make it obvious.
Also the contact email address ([{{site.email}}][5]) and the hashtag [#DRCOL][6] should be accessible too .


[1]: https://duckduckgo.com/?q=%22Dr.+Col™%22+!g+nutrauhealth
[2]: https://www.drit.ml/cards/AI.html
[3]: https://duckduckgo.com/?q=%22Doctor.+Col%22+!g+nutrauhealth
[4]: http://google.com/search?q=%22Dr.+Col%22&start=270
[5]: http://google.com/search?q=email+%22{{site.email}}%22
[6]: https://duckduckgo.com/?q=%23DRCOL+!g

--&nbsp;
by michelc
