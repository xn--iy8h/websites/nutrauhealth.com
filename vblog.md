---
layout: page
title: Blog
blog:
 url: media.html
 menu: Videos
next:
 url: team.html
 menu: Team
---
# Latest Posts


{% for post in site.data.media.videos %}
<div class="youtube" data-embed="AqcjdkPMPJA"> 
    <!-- (2) the "play" button -->
    <div class="play-button"></div> 
</div>
{% endfor %}

<hr>

{% for post in site.posts %}{% assign len = post.url|size %}
{% unless page.draft %}
- #### {{post.date | date_to_string }}: [{{post.title}}]({{post.url | replace_first: '/',''}}) by {{post.author.name}}
  {{ post.content | strip_newlines | strip_html | truncatewords: 120 }} {% endunless %}
{% endfor %}

<hr>

{% comment %}
{% for post in site.posts %}{% assign len = post.url|size %}
   {{post | jsonify}}
{% endfor %}
{% endcomment %}

